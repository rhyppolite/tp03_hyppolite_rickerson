package com.rzhyppolite.gmail.gestion.tp03_hyppolite_rickerson;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button gmBtn, apBtn,ggBtn,tpBtn;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gmBtn = findViewById(R.id.btn1);
        apBtn = findViewById(R.id.btn2);
        ggBtn = findViewById(R.id.btn3);
        tpBtn = findViewById(R.id.btn4);

        initViews();
    }

    private void initViews() {

            gmBtn.setOnClickListener(view -> {

                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_SUBJECT, "Test");
                intent.putExtra(Intent.EXTRA_TEXT,"Ce message concerne tous les étudiants du MBDS.");
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                    Toast.makeText(MainActivity.this, "Gmail", Toast.LENGTH_SHORT).show();
                }

           });


        apBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                startActivity(intent);
                Toast.makeText(MainActivity.this, "Call", Toast.LENGTH_SHORT).show();
            }
        });

        ggBtn.setOnClickListener(v -> {
            Intent intent = new Intent (Intent.ACTION_VIEW, Uri.parse("http://stedh.org"));
            startActivity(intent);
            Toast.makeText(MainActivity.this, "Google Chrome", Toast.LENGTH_SHORT).show();
        });

           tpBtn.setOnClickListener(v -> {
               Intent intent = new Intent("gestion.mbds.CALC");
               startActivity(intent);
           Toast.makeText(MainActivity.this, "TP01", Toast.LENGTH_SHORT).show();

        });

    }
}